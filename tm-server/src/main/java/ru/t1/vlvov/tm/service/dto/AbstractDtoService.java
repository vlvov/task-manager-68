package ru.t1.vlvov.tm.service.dto;

import ru.t1.vlvov.tm.api.service.dto.IDtoService;
import ru.t1.vlvov.tm.dto.model.AbstractModelDTO;

public abstract class AbstractDtoService<M extends AbstractModelDTO> implements IDtoService<M> {

}
