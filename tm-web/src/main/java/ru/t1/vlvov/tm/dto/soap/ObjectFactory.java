package ru.t1.vlvov.tm.dto.soap;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {

    }

    public ProjectCreateRequest createProjectCreateRequest() {
        return new ProjectCreateRequest();
    }

    public ProjectCreateResponse createProjectCreateResponse() {
        return new ProjectCreateResponse();
    }

    public ProjectDeleteByIdRequest createProjectDeleteByIdRequest() {
        return new ProjectDeleteByIdRequest();
    }

    public ProjectDeleteByIdResponse createProjectDeleteByIdResponse() {
        return new ProjectDeleteByIdResponse();
    }

    public ProjectDeleteRequest createProjectDeleteRequest() {
        return new ProjectDeleteRequest();
    }

    public ProjectDeleteResponse createProjectDeleteResponse() {
        return new ProjectDeleteResponse();
    }

    public ProjectFindByIdRequest createProjectFindByIdRequest() {
        return new ProjectFindByIdRequest();
    }

    public ProjectFindByIdResponse createProjectFindByIdResponse() {
        return new ProjectFindByIdResponse();
    }

    public ProjectFindRequest createProjectFindRequest() {
        return new ProjectFindRequest();
    }

    public ProjectFindResponse createProjectFindResponse() {
        return new ProjectFindResponse();
    }

    public TaskCreateRequest createTaskCreateRequest() {
        return new TaskCreateRequest();
    }

    public TaskCreateResponse createTaskCreateResponse() {
        return new TaskCreateResponse();
    }

    public TaskDeleteByIdRequest createTaskDeleteByIdRequest() {
        return new TaskDeleteByIdRequest();
    }

    public TaskDeleteByIdResponse createTaskDeleteByIdResponse() {
        return new TaskDeleteByIdResponse();
    }

    public TaskDeleteRequest createTaskDeleteRequest() {
        return new TaskDeleteRequest();
    }

    public TaskDeleteResponse createTaskDeleteResponse() {
        return new TaskDeleteResponse();
    }

    public TaskFindByIdRequest createTaskFindByIdRequest() {
        return new TaskFindByIdRequest();
    }

    public TaskFindByIdResponse createTaskFindByIdResponse() {
        return new TaskFindByIdResponse();
    }

    public TaskFindRequest createTaskFindRequest() {
        return new TaskFindRequest();
    }

    public TaskFindResponse createTaskFindResponse() {
        return new TaskFindResponse();
    }

}
