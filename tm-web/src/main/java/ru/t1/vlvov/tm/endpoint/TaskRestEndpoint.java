package ru.t1.vlvov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.vlvov.tm.api.ITaskRestEndpoint;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.service.TaskService;

import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    TaskService taskService;

    @Override
    @PostMapping("/save")
    public Task save(
            @RequestBody Task task
    ) {
        taskService.add(task);
        return task;
    }

    @Override
    @PostMapping("/delete")
    public void delete(
            @RequestBody Task task
    ) {
        taskService.remove(task);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @PathVariable("id") String id
    ) {
        taskService.removeById(id);
    }

    @Override
    @GetMapping("/findAll")
    public Collection<Task> findAll() {
        return taskService.findAll();
    }

    @Override
    @GetMapping("/findById/{id}")
    public Task findById(
            @PathVariable("id") String id
    ) {
        return taskService.findOneById(id);
    }


}
