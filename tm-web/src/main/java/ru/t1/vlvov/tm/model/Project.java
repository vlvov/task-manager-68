package ru.t1.vlvov.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.vlvov.tm.enumerated.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tm_project")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "project", propOrder = {
        "id",
        "name",
        "description",
        "status",
        "created"
})

public class Project {

    @NotNull
    @Id
    @XmlElement
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column
    @XmlElement
    private String name;

    @NotNull
    @Column
    @XmlElement
    private String description;

    @NotNull
    @Column
    @XmlElement
    @Enumerated(EnumType.STRING)
    @XmlSchemaType(name = "string")
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column
    @XmlElement
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    public Project(@NotNull final String name) {
        this.name = name;
    }

}
