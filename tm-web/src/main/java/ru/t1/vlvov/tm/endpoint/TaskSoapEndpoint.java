package ru.t1.vlvov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.vlvov.tm.dto.soap.*;
import ru.t1.vlvov.tm.service.TaskService;

@Endpoint
public class TaskSoapEndpoint {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public final static String NAMESPACE = "http://tm.vlvov.t1.ru/dto/soap";

    @Autowired
    TaskService taskService;

    @ResponsePayload
    @PayloadRoot(localPart = "taskCreateRequest", namespace = NAMESPACE)
    public TaskCreateResponse save(
            @RequestPayload final TaskCreateRequest request
    ) {
        taskService.add(request.getTask());
        return new TaskCreateResponse(request.getTask());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    public TaskDeleteResponse delete(
            @RequestPayload final TaskDeleteRequest request
    ) {
        taskService.remove(request.getTask());
        return new TaskDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(
            @RequestPayload final TaskDeleteByIdRequest request
    ) {
        taskService.removeById(request.getId());
        return new TaskDeleteByIdResponse("true");
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindRequest", namespace = NAMESPACE)
    public TaskFindResponse findAll(
            @RequestPayload final TaskFindRequest request
    ) {
        return new TaskFindResponse(taskService.findAll());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(
            @RequestPayload final TaskFindByIdRequest request
    ) {
        return new TaskFindByIdResponse(taskService.findOneById(request.getId()));
    }

}
