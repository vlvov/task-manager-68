package ru.t1.vlvov.tm.api;

import ru.t1.vlvov.tm.model.Task;
import java.util.Collection;

public interface ITaskRestEndpoint {

    Task save(Task task);

    void delete(Task task);

    void deleteById( String id);

    Collection<Task> findAll();

    Task findById(String id);

}
