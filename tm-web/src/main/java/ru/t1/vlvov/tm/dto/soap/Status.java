package ru.t1.vlvov.tm.dto.soap;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "status")
@XmlEnum
public enum Status {

    NOT_STARTED,
    IN_PROGRESS,
    COMPLETED;

    public static Status fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}

