package ru.t1.vlvov.tm.api;

import ru.t1.vlvov.tm.model.Project;
import java.util.Collection;

public interface IProjectRestEndpoint {

    Project save(Project project);

    void delete(Project project);

    void deleteById(String id);

    Collection<Project> findAll();

    Project findById(String id);

}
