package ru.t1.vlvov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.vlvov.tm.dto.soap.*;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.service.ProjectService;

import java.util.Collection;

@Endpoint
public class ProjectSoapEndpoint {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public final static String NAMESPACE = "http://tm.vlvov.t1.ru/dto/soap";

    @Autowired
    ProjectService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "projectCreateRequest", namespace = NAMESPACE)
    public ProjectCreateResponse save(
            @RequestPayload final ProjectCreateRequest request
    ) {
        projectService.add(request.getProject());
        return new ProjectCreateResponse(request.getProject());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse delete(
            @RequestPayload final ProjectDeleteRequest request
    ) {
        projectService.remove(request.getProject());
        return new ProjectDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(
            @RequestPayload final ProjectDeleteByIdRequest request
    ) {
        projectService.removeById(request.getId());
        return new ProjectDeleteByIdResponse("true");
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindRequest", namespace = NAMESPACE)
    public ProjectFindResponse findAll(
            @RequestPayload final ProjectFindRequest request
    ) {
        return new ProjectFindResponse(projectService.findAll());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(
            @RequestPayload final ProjectFindByIdRequest request
    ) {
        return new ProjectFindByIdResponse(projectService.findOneById(request.getId()));
    }

}
